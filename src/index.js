const express = require('express');
const app = express();
const morgan = require('morgan');
const path = require('path');
const bodyParser = require('body-parser');

app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: false })); 

//settings
app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs'); 
app.set('views', path.join(__dirname,'/views'))
app.engine('html', require('ejs').renderFile) 

//middlewares
app.use(morgan('dev'))

//routes
app.use(require('./routes/routes'))

//Static Files
app.use(express.static(path.join(__dirname,'public')))

//Inicializando servidor 
app.listen(app.get('port'),() => {
    console.log(`Server corriendo en el puerto ${app.get('port')}`)
})